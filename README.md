# MnasatTask

Mnasat Task app is a task for mnasat company for testing my iOS skills and experience.


## Prerequisites

- Mac running macOS 10.13.6 or later.
- Xcode 10.0

## Getting Started

- If cocoapods isn't installed on your machine, go to https://cocoapods.org/ and see how to install it.

- Open terminal app

- In terminal app run the following:
    1- cd "the app folder which contains Podfile"
    2- pod install
    
- Go to project folder and open 'Mnasat Task.xcworkspace' file.
- Enjoy


