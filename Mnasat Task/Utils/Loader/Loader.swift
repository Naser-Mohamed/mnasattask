//
//  Loader.swift
//  Mnasat Task
//
//  Created by Naser Mohamed on 1/14/19.
//  Copyright © 2019 Naser Mohamed. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class Loader: NSObject {
    static let shared = Loader()
    
    private var activityData:ActivityData{
        return ActivityData(size: CGSize(), message: "", type: NVActivityIndicatorType.ballRotateChase, color: UIColor.white, padding: 20, displayTimeThreshold: 10, minimumDisplayTime: 1)
    }
    
    func startAnimating() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
    }
    
    func stopAnimating() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
}
