//
//  ImagesResponse.swift
//  Mnasat Task
//
//  Created by Naser Mohamed on 1/14/19.
//  Copyright © 2019 Naser Mohamed. All rights reserved.
//

import UIKit

struct GetImagesResponse: Codable {
    var profiles: [ProfileImage] = []
}

struct ProfileImage: Codable {
    var file_path: String!
    var vote_count: Int!
}
