//
//  Person.swift
//  Mnasat Task
//
//  Created by Naser Mohamed on 1/13/19.
//  Copyright © 2019 Naser Mohamed. All rights reserved.
//

import UIKit

struct Person: Codable {
    var name: String!
    var id: Int!
    var profile_path: String!
    var adult: Bool!
    var popularity: Double!
    var biography: String!
    var place_of_birth: String!
    var birthday: String!
    
}

struct PopularPeopleResponse: Codable {
    var page: Int
    var total_results: Int
    var total_pages: Int
    var results: [Person]
}


