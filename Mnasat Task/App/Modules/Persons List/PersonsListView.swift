//
//  PersonsListView.swift
//  Mnasat Task
//
//  Created by Naser Mohamed on 1/13/19.
//  Copyright © 2019 Naser Mohamed. All rights reserved.
//

import UIKit

protocol PersonsListDelegate: class {
    func didSelect(person: Person, image: UIImage?)
    func noResultsFound()
}

extension PersonsListDelegate {
    func noResultsFound() {}
}

class PersonsListView: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDataLbl: UILabel!
    
    let refreshControl = UIRefreshControl()
    
    var viewModel: PersonsListViewModel!
    weak var delegate: PersonsListDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareTableView()
        
        if viewModel.listType == .regular {
            Loader.shared.startAnimating()
            viewModel.getPeople()
        }
        
        viewModel.successHandler = { [weak self] in
            Loader.shared.stopAnimating()
            if (self?.refreshControl.isRefreshing)! {
                self?.refreshControl.endRefreshing()
            }
            self?.tableView.reloadData()
            if self?.viewModel.listType == .search &&
                (self?.viewModel.dataSource.isEmpty)! &&
                !(self?.viewModel.searchQuery.isEmpty)! {
                self?.noDataLbl.isHidden = false
            }
        }
        
        viewModel.reloadDataHandler = { [weak self] in
            self?.tableView.reloadData()
            self?.noDataLbl.isHidden = true
        }
        
        viewModel.failureHandler = { [weak self] (error) in
            Loader.shared.stopAnimating()
            if (self?.refreshControl.isRefreshing)! {
                self?.refreshControl.endRefreshing()
            }
            self?.showAlert(title: "Error", message: error)
        }
    }
    
    private func prepareTableView() {
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
        
        tableView.dataSource = viewModel
    }
    
    @objc private func refresh() {
        refreshControl.beginRefreshing()
        viewModel.dataSource.removeAll()
        viewModel.images.removeAll()
        tableView.reloadData()
        noDataLbl.isHidden = true
        viewModel.getPeople(isRefresh: true)
    }
}

extension PersonsListView: UITableViewDelegate, UIScrollViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.didSelect(person: viewModel.dataSource[indexPath.row], image: viewModel.images[indexPath.row])
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        // check to get next page
        if scrollView.contentOffset.y + scrollView.frame.size.height >= scrollView.contentSize.height && scrollView.contentOffset.y > 0 {
            viewModel.getPeople()
        }
    }
}
