//
//  PersonCell.swift
//  Mnasat Task
//
//  Created by Naser Mohamed on 1/13/19.
//  Copyright © 2019 Naser Mohamed. All rights reserved.
//

import UIKit

class PersonCell: UITableViewCell {

    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(with name: String, image: UIImage?) {
        nameLbl.text = name
        personImage.image = image
    }
}
