//
//  PersonsListViewModel.swift
//  Mnasat Task
//
//  Created by Naser Mohamed on 1/13/19.
//  Copyright © 2019 Naser Mohamed. All rights reserved.
//

import UIKit
import Alamofire

class PersonsListViewModel: NSObject {
    
    enum ListType {
        case regular, search
    }
    
    var dataSource: [Person] = []
    var images: [UIImage?] = []
    private var pageNumber = 0
    
    var listType = ListType.regular
    var searchQuery: String! {
        didSet {
            dataSource.removeAll()
            images.removeAll()
            reloadDataHandler()
            if !searchQuery.isEmpty {
                Loader.shared.startAnimating()
                getPeople()
            }
        }
    }
    
    var successHandler: ()->() = {}
    var reloadDataHandler: ()->() = {}
    var failureHandler: (String)->() = {_ in}
    
    init(with type: ListType) {
        listType = type
    }
    
    func getPeople(isRefresh: Bool = false) {
        pageNumber = isRefresh ? 1 : pageNumber + 1
        var url = Endpoints.popularPeople(page: pageNumber).url
        if listType == .search {
            guard let searchText = searchQuery.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else {
                return
            }
            url = Endpoints.search(query: searchText, page: pageNumber).url
        }
        HttpClient.execute(urlString: url) { [weak self] (data, error) in
            if let error = error {
                self?.failureHandler(error)
            }
            else {
                if let res = Mapping.jsonDecoder(data: data, object: PopularPeopleResponse.self) {
                    self?.dataSource.append(contentsOf: res.results)
                    self?.images.append(contentsOf: Array(repeating: nil, count: res.results.count))
                    self?.successHandler()
                }
            }
        }
    }
}

extension PersonsListViewModel: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PersonCell", for: indexPath) as! PersonCell
        let person = dataSource[indexPath.row]
        cell.setup(with: person.name, image: images[indexPath.row])
        
        if let image = images[indexPath.row] {
            cell.personImage.image = image
        }
        else if let imagePath = person.profile_path {
            let imageUrl = Endpoints.image(name: imagePath).url
            HttpClient.getImage(with: imageUrl) { [weak self] (image) in
                if let image = image {
                    if let _cell = tableView.cellForRow(at: indexPath) as? PersonCell {
                        _cell.personImage.image = image
                    }
                    self?.images[indexPath.row] = image
                }
            }
        }
        
        return cell
    }
}
