//
//  PopularPeopleView.swift
//  Mnasat Task
//
//  Created by Naser Mohamed on 1/14/19.
//  Copyright © 2019 Naser Mohamed. All rights reserved.
//

import UIKit

class PopularPeopleView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let listView = segue.destination as? PersonsListView {
            listView.viewModel = PersonsListViewModel(with: .regular)
            listView.delegate = self
        }
    }
}

extension PopularPeopleView: PersonsListDelegate {
    func didSelect(person: Person, image: UIImage?) {
        if let detailsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PersonDetailsView") as? PersonDetailsView {
            detailsVC.viewModel = PersonDetailsViewModel(with: person, image: image)
            navigationController?.pushViewController(detailsVC, animated: true)
        }
    }
}
