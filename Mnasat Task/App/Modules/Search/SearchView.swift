//
//  SearchView.swift
//  Mnasat Task
//
//  Created by Naser Mohamed on 1/14/19.
//  Copyright © 2019 Naser Mohamed. All rights reserved.
//

import UIKit

class SearchView: UIViewController {
    
    @IBOutlet weak var noDataLbl: UILabel!
    
    var searchController : UISearchController!
    var listView: PersonsListView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareSearchBar()
        self.navigationItem.hidesBackButton = true
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(backAction))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    @objc func backAction() {
        navigationController?.popViewController(animated: true)
    }
    
    private func prepareSearchBar() {
        self.searchController = UISearchController(searchResultsController:  listView)
        searchController.searchBar.delegate = self
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = true
        navigationItem.titleView = searchController.searchBar
        definesPresentationContext = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let listView = segue.destination as? PersonsListView {
            listView.viewModel = PersonsListViewModel(with: .search)
            listView.delegate = self
            self.listView = listView
        }
    }
}

extension SearchView: UISearchResultsUpdating, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
//        if searchController.searchBar.text!.isEmpty {
//            listView.viewModel.searchQuery = ""
//        }
//        noDataLbl.isHidden = true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        listView.viewModel.searchQuery = searchBar.text!
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        listView.viewModel.searchQuery = ""
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchController.searchBar.text!.isEmpty {
            listView.viewModel.searchQuery = ""
        }
    }
}

extension SearchView: PersonsListDelegate {
    func didSelect(person: Person, image: UIImage?) {
        if let detailsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PersonDetailsView") as? PersonDetailsView {
            detailsVC.viewModel = PersonDetailsViewModel(with: person, image: image)
            navigationController?.pushViewController(detailsVC, animated: true)
        }
    }
    
    func noResultsFound() {
//        view.bringSubviewToFront(noDataLbl)
//        noDataLbl.becomeFirstResponder()
//        noDataLbl.isHidden = false
    }
}
