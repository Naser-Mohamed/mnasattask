//
//  PersonDetailsView.swift
//  Mnasat Task
//
//  Created by Naser Mohamed on 1/14/19.
//  Copyright © 2019 Naser Mohamed. All rights reserved.
//

import UIKit

class PersonDetailsView: UIViewController {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var placeLbl: UILabel!
    @IBOutlet weak var birthDayLbl: UILabel!
    @IBOutlet weak var biographyTV: UITextView!
    @IBOutlet weak var biographyHeight: NSLayoutConstraint!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    
    var viewModel: PersonDetailsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.isNavigationBarHidden = true
        
        nameLbl.text = viewModel.person.name
        profileImage.image = viewModel.profileImage
        
        imagesCollectionView.dataSource = viewModel
        imagesCollectionView.delegate = viewModel
        
        Loader.shared.startAnimating()
        viewModel.getPersonDetails()
        
        viewModel.successHandler = { [weak self] (person) in
            Loader.shared.stopAnimating()
            self?.biographyTV.isHidden = false
            
            self?.placeLbl.text = person.place_of_birth
            if person.biography != nil && !person.biography.isEmpty {
                self?.biographyTV.text = person.biography
            }
            else {
                self?.biographyHeight.constant = 0.0
            }
            if person.birthday != nil && !person.birthday.isEmpty {
                self?.birthDayLbl.text = "Born on \(person.birthday!)"
            }
        }
        
        viewModel.failureHandler = { [weak self] (error) in
            Loader.shared.stopAnimating()
            self?.showAlert(title: "Error", message: error)
        }
        
        viewModel.profilesHandler = { [weak self] in
            Loader.shared.stopAnimating()
            self?.imagesCollectionView.reloadData()
        }
        
        viewModel.profileImageHandler = { [weak self] (img) in
            self?.profileImage.image = img
        }
    }
    
    @IBAction func backBtnDidClick(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}
