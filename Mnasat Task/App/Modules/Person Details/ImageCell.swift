//
//  ImageCell.swift
//  Mnasat Task
//
//  Created by Naser Mohamed on 1/14/19.
//  Copyright © 2019 Naser Mohamed. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    
    func setImage(image: UIImage?) {
        self.image.image = image
    }
}
