//
//  PersonDetailsViewModel.swift
//  Mnasat Task
//
//  Created by Naser Mohamed on 1/14/19.
//  Copyright © 2019 Naser Mohamed. All rights reserved.
//

import UIKit

class PersonDetailsViewModel: NSObject {

    var person: Person!
    var profileImage: UIImage!
    var profiles: [ProfileImage] = [] {
        didSet {
            profilesHandler()
            images = Array(repeating: nil, count: profiles.count)
        }
    }
    var images: [UIImage?] = []
    
    var profilesHandler: ()->() = {}
    var successHandler: (Person)->() = {_ in}
    var failureHandler: (String)->() = {_ in}
    var profileImageHandler: (UIImage)->() = {_ in}
    
    init(with person: Person, image: UIImage?) {
        super.init()
        self.person = person
        if image == nil && person.profile_path != nil {
            let imageUrl = Endpoints.image(name: person.profile_path).url
            HttpClient.getImage(with: imageUrl) { [weak self] (image) in
                if let img = image {
                    self?.profileImage = img
                    self?.profileImageHandler(img)
                }
            }
        }
        else {
            self.profileImage = image
        }
    }
    
    func getPersonDetails() {
        getImages()
        let url = Endpoints.personDetails(id: person.id).url
        HttpClient.execute(urlString: url) { [weak self] (data, error) in
            if let error = error {
                self?.failureHandler(error)
            }
            else {
                if let res = Mapping.jsonDecoder(data: data, object: Person.self) {
                    self?.updateModel(with: res)
                    self?.successHandler((self?.person)!)
                }
            }
        }
    }
    
    func getImages() {
        let url = Endpoints.images(id: person.id).url
        HttpClient.execute(urlString: url) { [weak self] (data, error) in
            if let error = error {
                self?.failureHandler(error)
            }
            else {
                if let res = Mapping.jsonDecoder(data: data, object: GetImagesResponse.self) {
                    self?.profiles = res.profiles
                }
            }
        }
    }
    
    private func updateModel(with person: Person) {
        self.person.birthday = person.birthday
        self.person.biography = person.biography
        self.person.place_of_birth = person.place_of_birth
    }
}

extension PersonDetailsViewModel: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return profiles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCell
        cell.setImage(image: images[indexPath.item])
        
        if let image = images[indexPath.item] {
            cell.setImage(image: image)
        }
        else if let imagePath = profiles[indexPath.item].file_path {
            let imageUrl = Endpoints.image(name: imagePath).url
            HttpClient.getImage(with: imageUrl) { [weak self] (image) in
                if let image = image {
                    if let _cell = collectionView.cellForItem(at: indexPath) as? ImageCell {
                        _cell.setImage(image: image)
                    }
                    self?.images[indexPath.item] = image
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.width - 2) / 3
        return CGSize(width: width, height: width)
    }
}
