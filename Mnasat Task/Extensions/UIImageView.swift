//
//  UIImageView.swift
//  Mnasat Task
//
//  Created by Naser Mohamed on 1/14/19.
//  Copyright © 2019 Naser Mohamed. All rights reserved.
//

import UIKit

@IBDesignable
extension UIButton {
    @IBInspectable var imageTintColor: UIColor {
        set {
            let tintedImage = self.currentImage?.withRenderingMode(.alwaysTemplate)
            self.setImage(tintedImage, for: .normal)
            self.tintColor = newValue
        }
        get {
            return self.tintColor
        }
    }
    
//    func setTintColor(color: UIColor) {
//
//    }
}
