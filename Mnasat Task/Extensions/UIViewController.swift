//
//  UIViewController.swift
//  Mnasat Task
//
//  Created by Naser Mohamed on 1/14/19.
//  Copyright © 2019 Naser Mohamed. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlert(title: String, message: String, completionHandler: ((Bool) -> ())? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { _ in
            if completionHandler != nil {
                completionHandler!(true)
            }
        })
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}
