//
//  Mapping.swift
//  CareerShowcase
//
//  Created by Naser Mohamed on 10/24/18.
//  Copyright © 2018 Naser Mohamed. All rights reserved.
//

import UIKit

class Mapping: NSObject {
    class func jsonDecoder<ObjectType: Codable>(data: Data?, object: ObjectType.Type) -> ObjectType? {
        guard let data = data else { return nil}
        do {
            let decodedValue = try JSONDecoder().decode(object, from: data)
            return decodedValue
            
        } catch {
            return nil
        }
    }
}
