//
//  AppDelegate.swift
//  Mnasat Task
//
//  Created by Naser Mohamed on 1/13/19.
//  Copyright © 2019 Naser Mohamed. All rights reserved.
//

import UIKit

fileprivate struct API {
    static let baseUrl = "https://api.themoviedb.org/3"
    static let imageBaseUrl = "http://image.tmdb.org/t/p/original"
    static let apiKey = "a9be6d9cfba514329d83a2035bef4b3c"
}

protocol Endpoint {
    var path: String { get }
    var url: String { get }
}


enum Endpoints: Endpoint {
    
    case popularPeople(page: Int)
    case personDetails(id: Int)  // takes person id
    case image(name: String)    // takes image name
    case search(query: String, page: Int)
    case images(id: Int)
    
    var path: String {
        switch self {
        case .popularPeople:
            return "/person/popular"
        case .personDetails:
            return "/person"
        case .search:
            return "/search/person"
        default:
            return ""
        }
    }
    
    var url: String {
        switch self {
        case .popularPeople(let page):
            return "\(API.baseUrl)\(path)?api_key=\(API.apiKey)&page=\(page)"
        case .personDetails(let id):
            return "\(API.baseUrl)\(path)/\(id)?api_key=\(API.apiKey)"
        case .image(let name):
            return "\(API.imageBaseUrl)\(name)"
        case .search(let query, let page):
            return "\(API.baseUrl)\(path)?api_key=\(API.apiKey)&query=\(query)&page=\(page)"
        case .images(let id):
            return "\(API.baseUrl)/person/\(id)/images?api_key=\(API.apiKey)"
        }
    }
}
