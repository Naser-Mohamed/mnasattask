//
//  HttpClient.swift
//  CareerShowcase
//
//  Created by Naser Mohamed on 10/24/18.
//  Copyright © 2018 Naser Mohamed. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class HttpClient: NSObject {
    
    class func execute(urlString: String, parameters: [String: Any] = [:], encoding: ParameterEncoding = JSONEncoding.default, method: HTTPMethod = .get, headers: HTTPHeaders? = nil, completionHandler: @escaping (_ data: Data?, _ error: String?) -> ()) {
        let url = URL(string: urlString)
        Alamofire.request(url!).responseData { (response) in
            switch response.result {
            case .success(_):
                completionHandler(response.result.value, nil)
            case .failure(_):
                completionHandler(nil, response.result.error?.localizedDescription)
            }
        }
    }
    
    class func getImage(with url: String, completionHandler: @escaping (_ image: UIImage?) -> ()) {
        
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                completionHandler(image)
            }
        }
    }
}
